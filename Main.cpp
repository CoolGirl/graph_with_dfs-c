#include"Graph.h"
#include <stack>
#include <iostream>

int main(int argc, char* argv[])
{
	Graph<int> graph;
	graph.loadFromFile("input.txt");
	graph.saveToFile("output.txt");
	typedef Graph<int>::NodeHandle Node;
	std::stack<Node> answer;
	graph.dfs([](Node n){}, [&](Node n) {answer.push(n); }, [](Node n){});
	while (!answer.empty())
	{
		Node n = answer.top();
		std::cout << n << " ";
		answer.pop();
	}
	std::cout << "\n";
	std::cout << graph[1] << "\n";
	std::cout << graph.move(1, 6) << "\n";
	std::cout << graph.getNodesCount() << "\n";
	graph.forEachEdge(2, [](Node n) {std::cout << n << " "; });
	std::cout << "\n";
	graph.forEachNode([&](Node n) {std::cout << graph[n] << " "; });
	auto node = graph.addNode();
	std::cout << "\n";
	graph.forEachNode([&](Node n) {std::cout << graph[n] << " "; });
	std::cout << "\n" << graph.getNodesCount() << "\n";
	graph.addEdge(1, 3);
	graph.addEdge(node, node);
	graph.forEachEdge(node, [](Node n) {std::cout << n << " "; });
	std::cout << "\n";
}