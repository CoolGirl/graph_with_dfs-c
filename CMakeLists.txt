cmake_minimum_required(VERSION 2.8.4)

project(graph)

file(GLOB sources
    "*.h"
    "*.cpp"
)

add_definitions(-std=c++11)

add_executable(program ${sources})